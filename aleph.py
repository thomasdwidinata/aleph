#!/usr/bin/env python

# Created by Thomas Dwi Dinata

import curses
import subprocess
import logging
from time import sleep
from os import getuid, system

logging.basicConfig(filename='aleph.log',
    filemode='a',
    format='%(asctime)s,%(msecs)d %(name)s %(levelname)s %(message)s',
    datefmt='%H:%M:%S',
    level=logging.DEBUG)
logging.info("Running Aleph...")
subprocess_running = False

def main(stdscr):
    curses.curs_set(0)
    stdscr.clear()

    # --- START EDITING FROM HERE IF YOU NEED ANY ADDITIONAL SERVICES!!! ---
    apps = [
        {
            "name": "VSCode",
            "cmd": ['systemctl', 'start/stop', 'vscode.service'],
            "isDaemon": True,
            "description": "OpenVSCode server running at port 3000. Firewall only allowing access only from 10.55.0.0/29",
            "toggle": False
        },
        {
            "name": "VNC Server",
            "cmd": ['systemctl', 'start/stop', 'vncserver-x11-serviced.service'],
            "isDaemon": True,
            "description": "VNC server for accessing RPi's Desktop. Running on default port and can only be accessed from 10.55.0.0/29 (Firewall and VNC Config)",
            "toggle": False
        },
        {
            "name": "Desktop Environment (Best not to disable for stability)",
            "cmd": ['systemctl', 'start/stop', 'lightdm.service'],
            "isDaemon": True,
            "description": "The Raspberry Pi's desktop environment using Lightdm. Best not to disable this unless you only working with shell",
            "toggle": False
        },
        {
            "name": "MariaDB",
            "cmd": ['systemctl', 'start/stop', 'mariadb.service'],
            "isDaemon": True,
            "description": "MariaDB database server. Disabled at startup. If used, you can enable it from here",
            "toggle": False
        },
        {
            "name": "LED LERP",
            "cmd": ["python", "/home/thomasdd/led_lerp.py", "-d"],
            "isDaemon": False,
            "description": "Run the LED LERP in background by running simple Python script",
            "toggle": False
        }
    ]
    # --- END OF EDITABLE CONTENT ---

    # Set up colors (if supported by the terminal)
    curses.start_color()
    curses.init_pair(1, curses.COLOR_WHITE, curses.COLOR_BLUE)

    selected_item = 1

    # toggle_status is automatically iterated and set
    for app in apps:
        if app["isDaemon"] and system('systemctl is-active --quiet ' + app['cmd'][2]) == 0:
            app["toggle"] = True

    def clear_and_show_output(output, timeout=0):
        stdscr.clear()
        stdscr.addstr(5, 10, output)
        stdscr.refresh()
        sleep(timeout)

    # def toggle_function(app_order_num = 0):
    def toggle_function(apps_index = 0):
        nonlocal apps
        global subprocess_running

        apps_index -= 1

        # Clear screen for authentication and systemctl status monitor
        clear_and_show_output('Loading...')
        apps[apps_index]["toggle"] = not apps[apps_index]["toggle"]
        if apps[apps_index]['toggle'] and apps[apps_index]["isDaemon"]:
            apps[apps_index]['cmd'][1] = 'start'
            logging.debug('DAEMON & OFF, START... ' + ' '.join(apps[apps_index]['cmd']))
            subprocess.run(apps[apps_index]['cmd'])
        elif apps[apps_index]["isDaemon"]:
            apps[apps_index]['cmd'][1] = 'stop'
            logging.debug('DAEMON & ON, STOP... ' + ' '.join(apps[apps_index]['cmd']))
            subprocess.run(apps[apps_index]['cmd'])
        else:
            logging.debug('NORMAL CMD, running cmd... ' + ' '.join(apps[apps_index]['cmd']))
            subprocess_running = True
            P = subprocess.Popen(apps[apps_index]["cmd"])
            logging.info("Process spawned with PID " + str(P.pid))
            clear_and_show_output('Process spawned with PID `' + str(P.pid) + '`', 3)

    while True:
        stdscr.clear()
        stdscr.addstr(1, 10, "Services Manager", curses.A_BOLD)

        toggle_text = {}
        toggle_attr = {}
        for index, app in enumerate(apps):
            if app["isDaemon"]:
                toggle_text[index] = app["name"] + f" {'[ON]' if app['toggle'] else '[OFF]'}" if selected_item == index+1 else app["name"] + f" {'[ON]' if app['toggle'] else '[OFF]'}"
            else:
                toggle_text[index] = app["name"] + f" ==>" if selected_item == index+1 else app["name"]
            toggle_attr[index] = curses.color_pair(1) if selected_item == index+1 else curses.A_NORMAL
            stdscr.addstr(5+(1*index), 10, toggle_text[index], toggle_attr[index])

        # Print the exit menu
        exit_text = "Exit [q]" if selected_item == len(apps)+1 else "Exit [q]"
        exit_attr = curses.color_pair(1) if selected_item == len(apps)+1 else curses.A_NORMAL
        stdscr.addstr(7+index, 10, exit_text, exit_attr)

        # Refresh the screen
        stdscr.refresh()

        # Get user input
        key = stdscr.getch()

        # Handle arrow key presses
        if key == curses.KEY_UP:
            selected_item = max(1, selected_item - 1)
        elif key == curses.KEY_DOWN:
            selected_item = min(len(apps)+1, selected_item + 1)
        elif key in [10, curses.KEY_ENTER, ord(' ')]:
            if selected_item >= len(apps)+1:
                break
            else:
                toggle_function(selected_item)

        # Exit the loop on 'q' key press
        if key == ord('q'):
            break

if __name__ == "__main__":
    if getuid() != 0:
        print("Please run this script as root!")
    else:
        curses.wrapper(main)

if subprocess_running:
    print('Some subprocess may still be running. Make sure to check it using `ps aux | grep python` command!')
# aleph

## Introduction

This Python script allows me to manage `systemctl` services by easily toggle defined services on the script. This script is currently developed as it and may evolved into more practical and features. This script only tested on Raspberry Pi running Raspberry Pi OS (64bit).

## Installing

Make sure you have Python installed. There are no pre-requisites and dependencies. You can just immediately run the script but you must be running it as `root`.
